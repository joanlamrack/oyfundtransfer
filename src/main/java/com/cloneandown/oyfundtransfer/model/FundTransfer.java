package com.cloneandown.oyfundtransfer.model;

import java.util.Random;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Entity
@Table(name = "fundtransfer", schema = "public")
public class FundTransfer {
    @Id
    protected int id;
    protected String status;
    protected String statusDescription;
    protected String reference;
    protected String description;
    protected Integer amount;
    protected String destinationCode;
    protected String destinationHolderName;
    protected String email;
    protected String senderId;
    protected String created;
    protected String destinationAccountNumber;

    protected FundTransfer() {
    }

    public FundTransfer(
            String status,
            String statusDescription,
            String reference,
            String description,
            Integer amount,
            String destinationCode,
            String destinationHolderName,
            String email,
            String senderId,
            String created,
            String destinationAccountNumber) {

        Random r = new Random();
        this.id = Math.abs(r.nextInt());
        this.status = status;
        this.statusDescription = statusDescription;
        this.reference = reference;
        this.description = description;
        this.amount = amount;
        this.destinationCode = destinationCode;
        this.destinationHolderName = destinationHolderName;
        this.email = email;
        this.senderId = senderId;
        this.created = created;
        this.destinationAccountNumber = destinationAccountNumber;
    }

    public FundTransfer(
            int id,
            String status,
            String statusDescription,
            String reference,
            String description,
            Integer amount,
            String destinationCode,
            String destinationHolderName,
            String email,
            String senderId,
            String created,
            String destinationAccountNumber) {
        this.id = id;
        this.status = status;
        this.statusDescription = statusDescription;
        this.reference = reference;
        this.description = description;
        this.amount = amount;
        this.destinationCode = destinationCode;
        this.destinationHolderName = destinationHolderName;
        this.email = email;
        this.senderId = senderId;
        this.created = created;
        this.destinationAccountNumber = destinationAccountNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getDestinationCode() {
        return destinationCode;
    }

    public void setDestinationCode(String destinationCode) {
        this.destinationCode = destinationCode;
    }

    public String getDestinationHolderName() {
        return destinationHolderName;
    }

    public void setDestinationHolderName(String destinationHolderName) {
        this.destinationHolderName = destinationHolderName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getDestinationAccountNumber() {
        return destinationAccountNumber;
    }

    public void setDestinationAccountNumber(String destinationAccountNumber) {
        this.destinationAccountNumber = destinationAccountNumber;
    }

    public String toString() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "";
        }
    }
}