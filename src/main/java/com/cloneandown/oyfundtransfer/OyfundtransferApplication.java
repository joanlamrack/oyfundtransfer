package com.cloneandown.oyfundtransfer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OyfundtransferApplication {

	public static void main(String[] args) {
		SpringApplication.run(OyfundtransferApplication.class, args);
	}
}
