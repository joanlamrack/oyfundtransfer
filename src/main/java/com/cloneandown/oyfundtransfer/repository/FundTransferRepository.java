package com.cloneandown.oyfundtransfer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cloneandown.oyfundtransfer.model.FundTransfer;

@Repository
public interface FundTransferRepository extends JpaRepository<FundTransfer, Integer> {
    FundTransfer findById(int id);
}